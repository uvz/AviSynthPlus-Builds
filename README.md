## Avisynth+ builds

This repo contains Clang and IntelLLVM builds of [AviSynthPlus](https://github.com/AviSynth/AviSynthPlus).

The used source code does not contain anything different than the official github repository.

The main reason for these builds is to test the performance of the different builds.

Another reason for these builds is the long period between the officials release when meantime there are critical bugs fixed.<br>
Even ["the official" test builds](https://forum.doom9.org/showthread.php?t=181351) sometimes aren't published for longer period - for example, between `Avisynth+ 3.7.2 (20220317)` and `Avisynth+ 3.7.3 test 2 (20221114)` there are some critical bugs fixed ([1](https://github.com/AviSynth/AviSynthPlus/commit/459f533740b4570d352538a7c53373902510e909), [2](https://github.com/AviSynth/AviSynthPlus/commit/ad8472f822e352fbae5d89e2c747a6d2e26190ba), [3](https://github.com/AviSynth/AviSynthPlus/commit/746cc39d2df34726c02e13940d170fc01063423f)).

### Note

IntelLLVM builds < `r4029` are builded with `/fp:fast` that apparently can have unexpected behavior in some cases (for example, [post #2780](https://forum.doom9.org/showthread.php?p=1994621#post1994621)).

IntelLLVM builds > `r4073` are only x64 because `IntelLLVM 2025` dropped the x86 compiler.

### Updating

There is `avisynth_updater.bat` for easier update of AviSynth+. Usage:
- change default locations at the top of avisynth_updater.bat if the location of the plugins are different than the default `C:\Program Files (x86)\AviSynth+\plugins+` / `C:\Program Files (x86)\AviSynth+\plugins64+`;
- run avisynth_updater.bat as Administrator (it must be in the same folder alongside with the Clang folder and IntelLLVM folder) and choose what AviSynth+ build to use.

### Building

For `DirectShowSource` download [baseclasses](https://github.com/microsoft/Windows-classic-samples/tree/main/Samples/Win7Samples/multimedia/directshow/baseclasses) and build it (MBCS).<br>
Also [this line](https://github.com/microsoft/Windows-classic-samples/blob/main/Samples/Win7Samples/multimedia/directshow/baseclasses/wxdebug.cpp#L1237) must be changed to something like `CDisp p(pp);`.

SoundTorch building:

```
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DSOUNDSTRETCH=OFF -DSOUNDTOUCH_DLL=ON -DCMAKE_INSTALL_PREFIX=.\install_x64_icx -B build
ninja -C build
ninja -C build install
```

```
rem ver=x64
ver=Win32

rem ts=llvm
ts=intel c++ compiler 2025

git clone https://github.com/AviSynth/AviSynthPlus

cd AviSynthPlus

git checkout 3.7

git merge master

mkdir "build_%ts%_%ver%"

cd "build_%ts%_%ver%"

cmake -T "%ts%" -A %ver% -DCMAKE_CXX_FLAGS="/MP" -DCMAKE_PREFIX_PATH=soundtouch_installation_path -DIL_LIBRARIES=DevIL_lib_path -DILU_LIBRARIES=ILU_lib_path --DIL_INCLUDE_DIR=IL_include_path -DCMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES=soundtouch_include_path ..

rem For DirectShowSource -DBUILD_DIRECTSHOWSOURCE=ON must be added to the above command.
rem Also the hardcoded paths of baseclasses (https://github.com/AviSynth/AviSynthPlus/blob/master/plugins/DirectShowSource/CMakeLists.txt#L6-L12) must be changed.

rem For Intel builds you probably want to build against static Intel runtimes.
rem libmmds.lib and svml_disp.lib must be added to AdditionalDependencies in AvsCore.vcxproj (line ~198), PluginShibatch.vcxproj (line ~179), PluginTimeStretch.vcxproj (line ~179).

msbuild /m /p:Configuration=Release;Platform=%ver% "AviSynth+.sln"
```
