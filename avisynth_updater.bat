@echo off

rem -------- Change defaults --------

rem The location of the 32-bit plugins that will be updated:
set _default_location32=C:\Program Files (x86)\AviSynth+\plugins+

rem The location of the 64-bit plugins that will be updated:
set _default_location64=C:\Program Files (x86)\AviSynth+\plugins64+


rem -------- -------- -------- --------


:check1
set /p _version=Version to update (32bit, 64bit, all):
echo(%_version% | findstr /r /c:"^32bit $" /c:"^64bit $" /c:"^all $" 2> nul || (
    echo.
    echo ERROR! Wrong version.
	echo.
    goto :check1
)
echo.

:check2
set /p build_=What build to use (clang, intel):
echo(%build_% | findstr /r /c:"^clang $" /c:"^intel $" 2> nul || (
    echo.
    echo ERROR! Wrong build.
	echo.
    goto :check2
)
echo.

if "%build_%"=="clang" (
    set "build_loc_=%~dp0Clang"
) else (
    set "build_loc_=%~dp0IntelLLVM"
)

if "%_version%"=="32bit" (
    set "_location32=%_default_location32%"
) else if "%_version%"=="64bit" (
    set "_location64=%_default_location64%"
) else (
set "_location32=%_default_location32%"
set "_location64=%_default_location64%"
)

if defined _location32 (
    copy /y "%build_loc_%\x86\plugins\*.*" "%_default_location32%"
    copy /y "%build_loc_%\x86\AviSynth.dll" "%systemroot%\SysWOW64"
    copy /y "%build_loc_%\x86\DevIL.dll" "%systemroot%\SysWOW64"
)

if defined _location64 (
    copy /y "%build_loc_%\x64\plugins\*.*" "%_default_location64%"
    copy /y "%build_loc_%\x64\AviSynth.dll" "%systemroot%\System32"
    copy /y "%build_loc_%\x64\DevIL.dll" "%systemroot%\System32"
)

echo Done.
echo.
pause
